"""
get the statistics of the alignments
true alignemnts coming from INDELIABLE
estimated alignments coming from three alignment tools
statistics includes ANHD, GAPPINESS, length
parameter:
targetDir: work directory for the model condition
NumTaxa: the number of taxa including the outgroup, for 4 taxa simulation, NumTaxa=5  [5, 9]
algn_class: the source of the alignments; [true_algn, fsa_algn, mafft_algn, clustalw2_algn]
"""
import numpy as np
from myFun import *
import sys

import os
targetDir = sys.argv[1]      # 'condition4A'
numTaxa = int(sys.argv[2])   # 5
algn_class = sys.argv[3]


ave_AHDN_list = []
ave_gap_list = []
ave_MSAlen_list = []
for i in range(20):
    print(i)
    if algn_class == 'true_algn':
        currDir = targetDir + '/REPLICATE-R' + str(i + 1) + '/trueAlignments'
    elif algn_class == 'fsa_algn':
        currDir = targetDir + '/REPLICATE-R' + str(i + 1) + '/fsa-estimatedAlignments'
    elif algn_class == 'mafft_algn':
        currDir = targetDir + '/REPLICATE-R' + str(i + 1) + '/mafft-estimatedAlignments'
    elif algn_class == 'clustalw2_algn':
        currDir = targetDir + '/REPLICATE-R' + str(i + 1) + '/clustalw2-estimatedAlignments'
    elif algn_class == 'clustalo_algn':
        currDir = targetDir + '/REPLICATE-R' + str(i + 1) + '/clustalo-estimatedAlignments'

    files = os.listdir(currDir)
    ANHD_list = []
    gappiness_list = []
    MSAlen_list = []
    for file in files:
        if file == '.DS_Store':
            continue
        elif os.path.getsize(currDir+'/'+file) == 0:
            print((currDir + '/' + file))
            continue
        else:
            fas2fasta(currDir+'/'+file, currDir+'/'+file)
            ANHD, gappiness, MSAlen = get_ANHD_gappiness(currDir+'/'+file, numTaxa)
            ANHD_list.append(ANHD)
            gappiness_list.append(gappiness)
            MSAlen_list.append(MSAlen)

    ave_AHDN_list.append(np.mean(ANHD_list))
    ave_gap_list.append(np.mean(gappiness_list))
    ave_MSAlen_list.append(np.mean(MSAlen_list))

ave_AHDN = np.mean(ave_AHDN_list)
ave_gap = np.mean(ave_gap_list)
ave_len = np.mean(ave_MSAlen_list)
print(ave_AHDN)
print(ave_gap)
print(ave_len)
