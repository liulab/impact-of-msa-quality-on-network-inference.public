"""
generate three different MSAs from the generated simulation sequences using three different alignment tools
inferred their gene tree sets based on the inferred alignments
parameters:
targetDir: work directory for the model condition
align_method: the name of the alignment tool; [mafft, fsa, clustalw2]
NumTaxa: the number of taxa including the outgroup, for 4 taxa simulation, NumTaxa=5
"""
import os
import sys
from myFun import *
targetDir = sys.argv[1]  # condition C
align_method_Dir = ''
align_method = sys.argv[2]
NumTaxa = int(sys.argv[3])  # 5, 9, 7
outgroup_str = sys.argv[4]  # 5,9, O
# trialCount = int(sys.argv[5])


if outgroup_str == '5':
    remains = ["1","2","3","4"]
elif outgroup_str == '9':
    remains = ["1", "2", "3", "4", "5", "6", "7", "8"]
elif outgroup_str == 'O':
    remains = ["L", "Q", "R", "A", "G", "C"]

NumTrials = 20
if align_method == 'mafft':
    align_method_Dir = "/mnt/ufs18/home-114/gaomeiju/software/guidance"
elif align_method == 'clustalw2':
    align_method_Dir = '/mnt/ufs18/home-114/gaomeiju/sync_pc/toolbins/clustalw-2.1'
elif align_method == 'fsa':
    align_method_Dir = '/mnt/ufs18/home-114/gaomeiju/sync_pc/toolbins/fsa/bin'
elif align_method == 'clustalo':
    align_method_Dir = '/mnt/ufs18/home-114/gaomeiju/sync_pc/toolbins/clustal_omega'

currDir = os.getcwd()
print(currDir)

for xx in range(NumTrials):
    trialCount = xx
    print(trialCount)
    path1 = targetDir + '/REPLICATE-R' + str(trialCount + 1)
    path5 = path1 + '/' + align_method + '-estimatedAlignments/'
    os.mkdir(path5)

    for count in range(1000):
        os.chdir('%s' % path1)
        # estimate alignments from sequences by MAFFT/FSA/clustalw
        if align_method == 'clustalw2':
            os.system('%s/%s -INFILE=sequences/sequence.fasta.%s -ALIGN -OUTFILE=%s.estimatedAlignment.fasta -OUTPUT=FASTA -OUTORDER=INPUT' % (align_method_Dir, align_method, str(count+1), align_method))
        elif align_method == 'clustalo':
            os.system("%s/%s -i sequences/sequence.fasta.%s -o %s.estimatedAlignment.fasta --outfmt=fasta" % (
                align_method_Dir, align_method, str(count + 1), align_method))
        else:
            os.system("%s/%s sequences/sequence.fasta.%s >%s.estimatedAlignment.fasta" % (
                align_method_Dir, align_method, str(count+1), align_method))

        # infer tree from the estiamted alignment by FastTree
        os.system('echo %s >>%s-FastTree.inferredTrees.order' % (count, align_method))
        os.system('/mnt/ufs18/home-114/gaomeiju/sync_pc/toolbins/FastTree -nt -nosupport '
                  '-gtr <%s.estimatedAlignment.fasta >>%s-FastTree.inferredTrees.order' % (align_method, align_method))

        os.system('/mnt/ufs18/home-114/gaomeiju/sync_pc/toolbins/FastTree -nt -nosupport '
                  '-gtr <%s.estimatedAlignment.fasta >>%s-FastTree.inferredTrees' % (align_method, align_method))

        # get estimated alignment files and move them to directory
        os.system('mv %s.estimatedAlignment.fasta %s-estimatedAlignments/%s.estimatedAlignment.fast.%s'
                  % (align_method, align_method, align_method, str(count+1)))

        # os.system('rm LOG.txt')
        # os.system('rm trees.txt')
        os.chdir('%s' % currDir)

    # rooted the unroot trees
    align_Fasttree = open(path1 + '/' + align_method + '-FastTree.inferredTrees', 'r').readlines()
    rooted_align_Fasttree_i = ''
    for z in range(len(align_Fasttree)):
        rooted_align_Fasttree_i += get_rooted_tree(align_Fasttree[z][:-1], outgroup_str, remains)

    with open(path1 + '/' + align_method + '-FastTree.inferredTrees.rooted', 'w') as f:
        f.write(rooted_align_Fasttree_i)







