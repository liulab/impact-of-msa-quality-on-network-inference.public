"""
generate simulation data with one or two reticulation events according to the model conditions
each model condition generates 20 independent replicates
infer true gene tree sets and estimated gene tree sets based on true alignments
parameters:
idl_prob: insertion and deletion rate
tree_height: model tree height
targetDir: work directory for each model condition
NumTaxa: the number of taxa including the outgroup, for 4 taxa simulation, NumTaxa=5  [5, 9]
migration_num: the number of the migration or reticulation events; [1, 2]
"""
import os
import sys
import linecache
import numpy as np
from myFun import *
# 4A, 4B
idl_prob = float(sys.argv[1])  # 0.05
tree_height = float(sys.argv[2])  # 0.8
targetDir = sys.argv[3]  #
NumTaxa = int(sys.argv[4])  # 5, 9
migration_num = int(sys.argv[5])  # 1 or 2
dev_flag = int(sys.argv[6])  # 1 deviation;  0 no deviation
deviation_factor = 2
if dev_flag == 1:
    dev = True
else:
    dev = False

outgroup_str = str(NumTaxa)
if outgroup_str == '5':
    remains = ["1","2","3","4"]
elif outgroup_str == '9':
    remains = ["1", "2", "3", "4", "5", "6", "7", "8"]

align_method_Dir = ''
NumTrials = 20
currDir = os.getcwd()
print(currDir)
tools_Dir = '/mnt/ufs18/home-114/gaomeiju/sync_pc/toolbins'

# extract the model tree from the r8s result
# modeltrees_lines = open(targetDir + '/r8s.result', 'r').readlines()
# modeltrees = ''
# t = 0
# for i in range(56, len(modeltrees_lines), 5):
#     begin_id = modeltrees_lines[i].index('(')
#     end_id = modeltrees_lines[i].index(';')
#     one_tree = modeltrees_lines[i][begin_id:end_id + 1]
#     one_tree = one_tree.replace('t', '')
#     modeltrees += str(t) + '\n' + one_tree + '\n'
#     t += 1
#
# with open(targetDir + '/model.trees', "w") as f:
#     f.write(modeltrees)

fileContent = linecache.getlines(targetDir + '/model.trees')  # file contains all species trees
# the file content used in Indelible
control1 = """/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  INDELible V1.03 control file - basic.txt                                       //
//                                                                                 //
//      A basic introduction to the structure of the INDELible control file.       //
//                                                                                 //
//      There are other examples in the file examples.html in the help folder      //
//      Any one of those examples will work if cut and paste into a control file.  //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

// It is useful to know that anything on a line after two forward slashes is ignored.

/*
   Another useful thing to know is that anything after a forward slash and star
   is ignored until INDELible sees a star followed by a forward slash later on.
*/

[TYPE] NUCLEOTIDE 1	//  EVERY control file must begin with a [TYPE] command.
			//  The word can be NUCLEOTIDE, AMINOACID or CODON depending
			//  on what kind of simulation you want to do but must be in
			//  upper case. The number can be 1 or 2 and chooses the
			//  algorithm that INDELible uses (see manuscript). Both give
			//  identical results but in some cases one is quicker.
			//  Other blocks and commands following this statement
			//  can come in any order you like.

[MODEL]    GTRexample        //  Evolutionary models are defined in [MODEL] blocks.
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018 	//  GTR: TC;TA;TG;CA;CG;AG;(a,b,c,d,e, f=1,)
  [statefreq] 0.311475 0.191363 0.300414 0.196748        	//  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER medium-gap.txt  //  User-Defined Distribution
  [indelrate] %s       //  insertion rate = 0.08 relative to substitution rate of 1

""" % idl_prob
control2 = """ [treedepth] %s
 
[PARTITIONS] pGTR [tree1 GTRexample 1000]      // tree 1, model GTRexample, root length of 1000
[SETTINGS]
 [output] FASTA                               // FASTA, NEXUS, PHYLIP or PHYLIPT
[EVOLVE]
 pGTR 1 GTRout


""" % tree_height
I_pop = ' '.join(['1'] * NumTaxa)
species_tree_height = 1.0
# generate gene trees
msstr_lines = ''
for i in range(NumTrials):
    trialCount = i
    print(trialCount)
    path1 = targetDir + '/REPLICATE-R' + str(trialCount + 1)
    path2 = path1 + '/control.files'
    path3 = path1 + '/sequences'
    path4 = path1 + '/trueAlignments'
    os.mkdir(path1)  # put final genetrees and sequence   INDELible need rooted tree
    os.mkdir(path2)
    os.mkdir(path3)
    os.mkdir(path4)
    # get random tree
    modeltree = fileContent[int(trialCount * 2 + 1)][:-1]  # i speciestree all info  fix the parental tree
    modeltree_tp = ete3.Tree(modeltree)
    h_tree_height = modeltree_tp.get_farthest_leaf()[1]  # get tree height
    rescale_modeltree = rescale_tree(modeltree, species_tree_height / h_tree_height)  # rescale to tree height 1.0
    # add outgroup
    half_h = 1*species_tree_height  #0.5*species_tree_height
    onehalf_h = 2*species_tree_height #1.5*species_tree_height
    outgroup_modeltree = '(' + rescale_modeltree[:-1] + ':' + str(half_h) + ',' + str(NumTaxa) + ':' + str(onehalf_h) + ');'
    mssplit = newick2ms(outgroup_modeltree)  # rescale= specified height/tree height
    msstr_lines += mssplit + '\n'
    # add reticulation events
    ms_str = ''  # the command string used in ms
    if migration_num == 1:  # add one migration event
        tM = np.random.uniform(0.01, 0.25 * species_tree_height)
        migration_str, from_node, to_node, tM = get_one_migration_population(outgroup_modeltree, tM, outgroup_str,
                                                                             mssplit, species_tree_height)
        with open(targetDir + '/migration' + str(trialCount + 1) + '.txt', "w", encoding="utf-8") as f:
            f.write(migration_str)
        # tools_Dir,
        ms_str = '''%s/ms %s 1000 -T -I %s %s %s -em %s %s %s 5.0 -em %s %s %s 0.0 > \
                        %s/REPLICATE-R%s/gene.trees.%s''' % (tools_Dir, str(NumTaxa), str(NumTaxa), I_pop, mssplit, str(tM - 0.01),
                                                             to_node, from_node, str(tM + 0.01),
                                                             to_node, from_node, targetDir, str(trialCount + 1),
                                                             str(trialCount + 1))

    elif migration_num == 2:  # add two non-deep migration events
        tM_1 = np.random.uniform(0.0, species_tree_height, 1)
        while True:
            tM_2 = np.random.uniform(0.0, species_tree_height, 1)
            if tM_2 != tM_1:
                break
        tM_list = [tM_1[0], tM_2[0]]
        outgroup_str = str(NumTaxa)
        migration_str, from_nodes, to_nodes, tM_list = get_nondeep_migrations(outgroup_modeltree, tM_list, outgroup_str,
                                                                              mssplit, species_tree_height)
        with open(targetDir + '/migration' + str(trialCount + 1) + '.txt', "w", encoding="utf-8") as f:
            f.write(migration_str)
        ms_str = '''%s/ms %s 1000 -T -I %s %s %s -em %s %s %s 5.0 -em %s %s %s 0.0 -em %s %s %s 5.0 -em %s %s %s 0.0 >%s/REPLICATE-R%s/gene.trees.%s''' % (
            tools_Dir,NumTaxa, NumTaxa, I_pop, mssplit, str(tM_list[0] - 0.01),
            to_nodes[0], from_nodes[0], str(tM_list[0] + 0.01),
            to_nodes[0], from_nodes[0], str(tM_list[1] - 0.01),
            to_nodes[1], from_nodes[1], str(tM_list[1] + 0.01),
            to_nodes[1], from_nodes[1],
            targetDir, str(trialCount + 1),
            str(trialCount + 1))

    os.system(ms_str)  # generate 1000 local gene trees
    genetrees_lines = open(path1 + '/gene.trees.' + str(trialCount + 1), 'r').readlines()
    control_file_k = ''
    count = 1
    true_genetrees = ''
    control_genetrees = ''
    # generate 1000 replicates of sequences using INDELIABLE
    for k in range(4, len(genetrees_lines), 3):
        if dev:
            deviation_tree = tree_deviation(genetrees_lines[k][:-1], deviation_factor, NumTaxa)
        else:
            deviation_tree = genetrees_lines[k][:-1]

        temp_str = '[TREE] tree1\n' + deviation_tree + '\n'
        true_genetrees += deviation_tree + '\n'
        control_file_k = control1 + temp_str + control2

        with open(path1 + '/control.txt', 'w') as f:
            f.write(control_file_k)

        # use INDELIABLE to generate the sequences and alignments
        os.chdir('%s' % path1)
        os.system('cp %s%s medium-gap.txt' % (tools_Dir, '/medium-gap.txt'))
        os.system('%s/indelible ' % tools_Dir)
        # infer tree from the true alignment
        # add the number
        os.system('echo %s >>true-FastTree.inferredTrees.order' % count)
        os.system('%s/FastTree -nt -nosupport '
                  '-gtr <GTRout_TRUE.fas >>true-FastTree.inferredTrees.order' % tools_Dir)

        os.system('%s/FastTree -nt -nosupport '
                  '-gtr <GTRout_TRUE.fas >>true-FastTree.inferredTrees' % tools_Dir)

        # get sequence and true.alignment files and move them to directory
        os.system('mv control.txt control.files/control.txt.%s' % str(count))
        os.system('mv GTRout_TRUE.fas trueAlignments/trueAlignment.fasta.%s' % str(count))
        os.system('mv GTRout.fas sequences/sequence.fasta.%s' % str(count))

        os.system('rm LOG.txt')

        tree_lines = open('trees.txt', 'r').readlines()
        if len(tree_lines) != 0:
            temp_list = list(map(str, re.split("[\t]", tree_lines[6][:-1])))
            tree_str = temp_list[8]
            control_genetrees += str(count) + '\t' + tree_str + '\n'

        os.system('rm trees.txt')
        count += 1
        os.chdir('%s' % currDir)

    with open(path1 + '/true.genetrees.unrooted', 'w') as f:
        f.write(true_genetrees)
    with open(path1 + '/control.genetrees', 'w') as f:
        f.write(control_genetrees)

    # root the unrooted trees
    true_trees = open(path1 + '/true.genetrees.unrooted', 'r').readlines()
    true_Fasttree = open(path1 + '/true-FastTree.inferredTrees', 'r').readlines()

    rooted_true_tree_i = ''
    rooted_true_Fasttree_i = ''
    for x in range(len(true_trees)):
        rooted_true_tree_i += get_rooted_tree(true_trees[x][:-1], outgroup_str, remains)
    for y in range(len(true_Fasttree)):
        rooted_true_Fasttree_i += get_rooted_tree(true_Fasttree[y][:-1], outgroup_str, remains)

    with open(path1 + '/true.genetrees.rooted', 'w') as f:
        f.write(rooted_true_tree_i)
    with open(path1 + '/true-FastTree.inferredTrees.rooted', 'w') as f:
        f.write(rooted_true_Fasttree_i)

with open(targetDir + '/ms_split_str.txt', 'w') as f:
    f.write(msstr_lines)
