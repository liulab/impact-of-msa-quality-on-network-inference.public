import ete3
import numpy as np
import sys
import re
from myFun import *

"""
the RF distance of unrooted tree with outgroup
"""
work_Dir = sys.argv[1]
aln_method = sys.argv[2]
numTaxa = sys.argv[3]   # 5, 9
tree_infer_method = sys.argv[4]   #  RAXML fasttree

outgroup = str(numTaxa)
if outgroup == '5':
    remains = ["1","2","3","4"]
elif outgroup == '9':
    remains = ["1", "2", "3", "4", "5", "6", "7", "8"]

Dist_MSA_list = []
Dist_mafft_list = []
Dist_fsa_list = []
Dist_clustalw2_list = []

if aln_method == 'true':
    flag = 'true'
elif aln_method == 'mafft':
    flag = 'mafft'
elif aln_method == 'fsa':
    flag = 'fsa'
elif aln_method == 'clustalw2':
    flag = 'clustalw2'
elif aln_method == 'clustalo':
    flag = 'clustalo'

if tree_infer_method == 'fasttree':
    tree_prefix = ''
elif tree_infer_method == 'RAXML':
    tree_prefix = '.RAXML'
for i in range(20):
    print(i)
    # preprocess the tree files
    trees_path_true_MSA = work_Dir + '/REPLICATE-R' + str(i + 1) + '/' + flag + '-FastTree.inferredTrees' + tree_prefix + '.order'
    trees_path_true_MSA1 = work_Dir + '/REPLICATE-R' + str(i + 1) + '/' + flag + '-FastTree.inferredTrees' + tree_prefix + '.order1'
    trees_true_MSA = open(trees_path_true_MSA, 'r').readlines()
    new_tree_lines1 = ''
    for t1 in range(len(trees_true_MSA)):
        if len(trees_true_MSA[t1]) <= 6 and len(trees_true_MSA[t1 - 1]) > 10:
            order_num = int(trees_true_MSA[t1][:-1])
            new_tree_lines1 += str(order_num) + '\t'
        elif len(trees_true_MSA[t1]) <= 6 and len(trees_true_MSA[t1 - 1]) <= 6:
            new_tree_lines1 += '\n'
            order_num = int(trees_true_MSA[t1][:-1]) + 1
            new_tree_lines1 += str(order_num) + '\t'
        else:   # tree
            new_tree_lines1 += trees_true_MSA[t1]
    with open(trees_path_true_MSA1, 'w') as f:
        f.write(new_tree_lines1)

    # get the RF distance
    trees_path_true_tree = work_Dir + '/REPLICATE-R' + str(i + 1) + '/true.genetrees.unrooted'
    trees_path_true_MSA = work_Dir + '/REPLICATE-R' + str(i + 1) + '/' + flag + '-FastTree.inferredTrees' + tree_prefix + '.order1'
    # print(trees_path_true_tree)
    # print(trees_path_true_MSA)
    # check if each file has the same number of tree
    trees_true_tree = open(trees_path_true_tree, 'r').readlines()
    trees_true_MSA = open(trees_path_true_MSA, 'r').readlines()
    dist_MSA_list = []
    Len1 = len(trees_true_tree)
    Len2 = len(trees_true_MSA)
    # print(Len1)
    # print(Len2)
    # if Len1 == Len2 == 1000:
    for t in range(1000):
        t_true = ete3.Tree(trees_true_tree[t][:-1])   # rooted tree
        # get the unrooted tree without outgroup
        t_true.set_outgroup(outgroup)
        t_true.prune(remains)

        temp_MSA = list(map(str, re.split("[\t]", trees_true_MSA[t][:-1])))
        if temp_MSA[1] != '':
            t_MSA = ete3.Tree(temp_MSA[1])
            t_MSA.set_outgroup(outgroup)
            t_MSA.prune(remains)
            dist_MSA = t_true.robinson_foulds(t_MSA, unrooted_trees=True)
            dist_MSA_list.append(dist_MSA[0]/dist_MSA[1])

    Dist_MSA_list.append(np.mean(dist_MSA_list))
    # else:
    #     print('error: ' + str(i))


print(np.mean(Dist_MSA_list))
print(standard_error(Dist_MSA_list))
