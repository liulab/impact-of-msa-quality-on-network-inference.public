"""
get all reduced distance between true network and estimated networks
MLE based estimated network, MPL based estimated networks:
1) network based on true gene tree sets
2) network based on estimated gene tree sets inferred from true alignments
3) network based on estimated gene tree sets inferred from mafft alignments
4) network based on estimated gene tree sets inferred from fsa alignments
5) network based on estimated gene tree sets inferred from clustalw2 alignments
parameters:
targetDir: work directory for the model condition
infer_method: the method using to infer the network, [ML, MPL]
migration_num: the number of the migration or reticulation events; [1, 2]
"""

import re
from myFun import *
import ete3
import os
import sys

targetDir = sys.argv[1]
infer_method = sys.argv[2]   # ML,  MPL， MP
migration_num = sys.argv[3]
tree_infer_method = sys.argv[4]     #  RAXML fasttree
align_method = sys.argv[5]   # true_tree, true_aln, mafft, fsa, clustalw2

line1="""#NEXUS
BEGIN NETWORKS;
"""

line2 = """
END;

BEGIN PHYLONET;

Cmpnets net1 net2 -m luay;

END;
"""
if tree_infer_method == 'fasttree':
    tree_prefix = ''
elif tree_infer_method == 'RAXML':
    tree_prefix = '.RAXML'

path1 = targetDir + '/model2.network'
model_networks = open(path1, 'r').readlines()

for i in range(20):

    print(i)
    # if i == 4:
    #     continue
    network1 = model_networks[i]
    path2 = targetDir + '/REPLICATE-R' + str(i + 1)
    if align_method == 'true_tree':
        network_temp = open(path2 + '/true.genereee.' + infer_method + '.' + migration_num + '.network', 'r').readlines()
    elif align_method == 'true_aln':
        network_temp = open(path2 + '/true-FastTree.' + infer_method + '.' + migration_num + '.network', 'r').readlines()
    else:  # mafft, fsa, clustalw2, clustalo
        network_temp = open(path2 + '/' + align_method + '-FastTree.' + infer_method + '.' + migration_num + tree_prefix +'.network', 'r').readlines()

    estimate_network = get_esttree(infer_method, network_temp)

    # remove the support value in the inferred networks
    estimate_network1 = delete_network_support(estimate_network)

    tree_net = 'net'
    script1 = line1 + 'Network ' + tree_net + '1 = ' + network1 + 'Network ' + tree_net + '2 = ' + estimate_network1 + line2

    with open(path2 + '/dist.' + infer_method + '.' + migration_num + '.' + align_method + tree_prefix + '.nex', 'w') as f:
        f.write(script1)

    os.system('java -jar PhyloNet.jar %s/dist.%s.%s.%s%s.nex >%s/%s-FastTree.%s.%s%s.network.dist' % (path2, infer_method, migration_num, align_method, tree_prefix, path2, align_method, infer_method, migration_num, tree_prefix))
