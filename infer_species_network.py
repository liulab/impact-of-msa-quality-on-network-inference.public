"""
infer the phylogenetic networks based on the input gene tree sets
parameters:
targetDir: work directory for the model condition
infer_method: the method using to infer the network, [ML, MPL]
migration_num: the number of the migration or reticulation events; [1, 2]
tree_sets_class: the source of the input gene tree sets; [true_tree, true_algn,  fsa, mafft, clustalw2]
"""
import os
import sys
import time
import numpy as np
from myFun import standard_error

targetDir = sys.argv[1]
infer_method = sys.argv[2]     # ML,  MPL,  MP, Bay
migration_num = sys.argv[3]
tree_sets_class = sys.argv[4]    # true_tree;  true_aln; fsa; mafft; clustalw2
tree_infer_method = sys.argv[5]   #  RAXML fasttree
trialCount = int(sys.argv[6])

NumTrials = 1
line1 = "#NEXUS\nBEGIN TREES;\n"
if infer_method == 'Bay':
    line2 = """END;

BEGIN PHYLONET;
MCMC_GT (all) -mr 1;
END;
"""
else:
    line2 = """END;
    
BEGIN PHYLONET;
InferNetwork_%s (all) %s -pl 1;
END;
""" % (infer_method, migration_num)

if tree_infer_method == 'fasttree':
    tree_prefix = ''
elif tree_infer_method == 'RAXML':
    tree_prefix = '.RAXML'
runtime_list = []
for x in range(NumTrials):
    # trialCount = x
    print(trialCount)
    path1 = targetDir + '/REPLICATE-R' + str(trialCount + 1)

    if tree_sets_class == 'true_tree':
        tree_set = open(path1 + '/true.genetrees.rooted', 'r').readlines()
        name_prefix = 'true.genereee'
    elif tree_sets_class == 'true_aln':
        tree_set = open(path1 + '/true-FastTree.inferredTrees.rooted', 'r').readlines()
        name_prefix = 'true-FastTree'
    else:
        align_method = tree_sets_class
        tree_set = open(path1 + '/' + align_method + '-FastTree.inferredTrees' + tree_prefix + '.rooted', 'r').readlines()
        name_prefix = align_method + '-FastTree'

    temp = ''
    for y in range(len(tree_set)):
        temp += 'Tree gt' + str(y) + ' = ' + tree_set[y]

    filelines = line1 + temp + line2

    with open(path1 + '/' + name_prefix + '.' + infer_method + '.' + migration_num + tree_prefix + '.nex', 'w') as f:
        f.write(filelines)

    begin_time = time.time()
    os.system('java -jar PhyloNet.jar %s/%s.%s.%s%s.nex >%s/%s.%s.%s%s.network' % (path1, name_prefix, infer_method, migration_num, tree_prefix, path1, name_prefix, infer_method, migration_num, tree_prefix))
    end_time = time.time()
    runtime = (end_time - begin_time) / 3600
    runtime_list.append(runtime)

mean_time = np.mean(runtime_list)
var_time = standard_error(runtime_list)
output_str = 'mean:\t' + str(mean_time) + '\n'
with open(targetDir + '/' + name_prefix + '.' + infer_method + '.' + migration_num + '.' + tree_prefix + '_' + str(trialCount) + '.txt', 'w') as f:
    f.write(output_str)
